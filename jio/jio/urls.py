"""jio URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from rest_framework.documentation import include_docs_urls
from user_management import views

# API
router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)

# Additional
'''
snippet_list = views.UserViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
snippet_detail = views.UserViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})
'''

user_group_detail = views.UserGroupViewSet.as_view({
    'put': 'add_group',
    'delete': 'remove_group'
})

urlpatterns = [
	url(r'^admin/', admin.site.urls),
    # Before router
    # Delete user by email
    url(r'^api/v1/users/(?P<email>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})/$', views.UserViewSet.as_view({'delete': 'delete_by_email'}), name='user-delete-detail'),
    # Delete group by name
    url(r'^api/v1/groups/(?P<group>[A-Za-z_][A-Za-z0-9_]*)/$', views.GroupViewSet.as_view({'delete': 'delete_by_name'}), name='group-delete-detail'),
	# Wire up our API using automatic URL routing.
	# Additionally, we include login URLs for the browsable API.
    url(r'^api/v1/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # Additional API's
    # User Suspend/Unsuspend
    url(r'^api/v1/users/(?P<pk>[0-9]+)/suspend/$', views.UserViewSet.as_view({'post': 'suspend', 'delete': 'unsuspend'}), name='user-suspend-detail'),
    url(r'^api/v1/users/(?P<pk>[0-9]+)/suspend/(?P<status>[0-9]+)/$', views.UserViewSet.as_view({'put': 'toggle_suspend'}), name='user-suspend-detail'),
    # Group Assign/Unassign
    url(r'^api/v1/users/(?P<pk>[0-9]+)/group/$', views.UserGroupViewSet.as_view({'post': 'change_group'}), name='user-group-detail'),
    url(r'^api/v1/users/(?P<pk>[0-9]+)/group/(?P<group>[0-9]+)/$', user_group_detail, name='user-group-detail'),
    # Login via email & password
    url(r'^api/v1/login/$', views.UserViewSet.as_view({'post': 'login'}), name='user-login'),
    url(r'^docs/', include_docs_urls(title='My API title'))
]
