from django.shortcuts import render

from django.contrib.auth.models import User, Group
from rest_framework import viewsets, status
from rest_framework.views import APIView
from user_management.serialize import *
from user_management.models import Profile
from rest_framework.response import Response
# Csrf
from user_management.authentication import CsrfExemptSessionAuthentication
from rest_framework.authentication import SessionAuthentication, BasicAuthentication 
# Permission
from rest_framework.permissions import AllowAny, IsAuthenticated
from user_management.permission import *
from rest_condition import And, Or, Not, C

class UserGroupViewSet(viewsets.ModelViewSet):
	queryset = User.objects.all()
	serializer_class = UserGroupAssignmentSerializer
	permission_classes = (IsAuthenticated, UserGroupPermission, )
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

	def group_handler(self, instance, request, user_group, overwrite = True, delete = False):
		if (type(user_group) == list):
			# Check if all group exist
			groups = Group.objects.filter(id__in=user_group)
			if (delete == False and len(user_group) != groups.count()):
				return 0
			else:
				if (delete):
					for to_add in groups:
						to_add.user_set.remove(instance)
					#instance.groups = [x for x in instance.groups if x not in user_group]
					return 1
				if (overwrite):
					instance.groups.clear()
				for to_add in groups:
					to_add.user_set.add(instance)
				return 1
		elif (type(user_group) == int or (type(user_group) == str and user_group.isdigit())):
			user_group = int(user_group)
			groups = Group.objects.filter(id=user_group)
			if (groups.count() != 1):
				return 0
			else:
				if (delete):
					groups.first().user_set.remove(instance)
					return 1
				if (overwrite):
					instance.groups.clear()
				groups.first().user_set.add(instance)
				return 1
		else:
			return -1
		return 1

	def change_group(self, request, *args, **kwargs):
		instance = self.get_object()
		user_group = request.data['groups']
		content = {
			'status': 'Success. Groups Changed'
		}
		retVal = self.group_handler(instance, request, user_group)
		if (retVal == -1):
			return Response({'status': 'Failed'}, status=status.HTTP_400_BAD_REQUEST)
		elif (retVal == 0):
			content['status'] = 'Invalid Groups were given as input. Please try again with correct groups'
		return Response(content)

	def add_group(self, request, *args, **kwargs):
		instance = self.get_object()
		user_group = kwargs.pop('group')
		content = {
			'status': 'Success. Groups Changed'
		}
		retVal = self.group_handler(instance, request, user_group, overwrite=False)
		if (retVal == -1):
			return Response({'status': 'Failed'}, status=status.HTTP_400_BAD_REQUEST)
		elif (retVal == 0):
			content['status'] = 'Invalid Groups were given as input. Please try again with correct groups'
		return Response(content)

	def remove_group(self, request, *args, **kwargs):
		instance = self.get_object()
		user_group = kwargs.pop('group')
		content = {
			'status': 'Success. Groups Changed'
		}
		retVal = self.group_handler(instance, request, user_group, overwrite=False, delete=True)
		if (retVal == -1):
			return Response({'status': 'Failed'}, status=status.HTTP_400_BAD_REQUEST)
		elif (retVal == 0):
			content['status'] = 'Invalid Groups were given as input. Please try again with correct groups'
		return Response(content)

class UserViewSet(viewsets.ModelViewSet):
	"""
	API endpoint that allows users to be viewed or edited.
	"""
	queryset = User.objects.all()
	serializer_class = UserSerializer
	#permission_classes = (IsAuthenticated, IsOwnerPermission, )
	# Should be authenticated or owner
	permission_classes = [C(IsAuthenticated) and (C(IsOwnerPermission) or C(UserPermission)), ]
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

	def login(self, request, *args, **kwargs):
		instance = request.user
		content = {
			'account_status': 'active' if instance.is_active else 'suspended'
		}
		return Response(content, status=status.HTTP_200_OK)

	def change_suspend(self, request, status):
		instance = self.get_object()
		if (instance.is_active == status):
			return False
		else:
			instance.is_active = status
			instance.save()
		return True

	def toggle_suspend(self, request, *args, **kwargs):
		content = {
			'status': 'Success. User Suspended',
		}
		if (self.change_suspend(request, False) == True):
			content['status'] = 'Success. User unsuspended'
		else:
			self.change_suspend(request, True)
		return Response(content)

	def suspend(self, request, *args, **kwargs):
		content = {
			'status': 'Success',
		}
		if (self.change_suspend(request, True) == False):
			content['status'] = 'Failed. User is already suspended'

		return Response(content)

	def unsuspend(self, request, *args, **kwargs):
		content = {
			'status': 'Success',
		}
		if (self.change_suspend(request, False) == False):
			content['status'] = 'Failed. User is already active'

		return Response(content)
	
	def delete_by_email(self, request, *args, **kwargs):
		email = kwargs.pop('email')
		content = {
			'status': 'Invalid User'
		}

		profile = Profile.objects.filter(email=email).first()
		if profile:
			# Get User by profile
			user = User.objects.filter(profile=profile).first()
			if user:
				user.delete()
				content['status'] = 'User Deleted'
				
		
		return Response(content, status=status.HTTP_200_OK)

	def create(self, request, *args, **kwargs):
		if (not self.permValue(request, c = 1)):
			return Response({'status': 'Permission Denied'}, status=status.HTTP_403_FORBIDDEN)
		return super().create(request, args, kwargs)

	def destroy(self, request, *args, **kwargs):
		if (not self.permValue(request, c = 1)):
			return Response({'status': 'Permission Denied'}, status=status.HTTP_403_FORBIDDEN)
		return super().destroy(request, args, kwargs)

	def update(self, request, *args, **kwargs):
		if (not self.permValue(request, c = 3)):
			return Response({'status': 'Permission Denied'}, status=status.HTTP_403_FORBIDDEN)
		partial = kwargs.pop('partial', False)
		instance = self.get_object()
		serializer = self.get_serializer(instance, data=request.data, partial=partial)
		if serializer.is_valid():
			# Update all fields
			try:
				self.perform_update(serializer)
				instance.profile.save()
			except Exception as e:
				# Cause: Duplicate Email
				# msg: User has no profile.
				content = serializer.data
				if (type(e) == str):
					content['status'] = e
				else:
					e = str(e)
					if (e == "UNIQUE constraint failed: user_management_profile.email"):
						content['status'] = 'Duplicate Email Address'
						content['profile'] = None
				return Response(content, status=status.HTTP_400_BAD_REQUEST)
			# Check Password
			profile = serializer.data.get("profile")
			if (profile.get("new_password") != None and profile.get("new_password") != ""):
				print(instance.password, instance.check_password(serializer.data.get("old_password")), "password")
				print(profile)
				if (instance.password == "" or instance.check_password(profile.get("old_password"))):
					instance.set_password(profile.get("new_password"))
					instance.save()
				else:
					content = serializer.data
					content['status'] = 'Wrong Password'
					profile['old_password'], profile['new_password'] = ['', '']
					# Don't send new password
					return Response(content, status=status.HTTP_400_BAD_REQUEST)
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
	
	# Override Methods
	def list(self, request, *args, **kwargs):
		if (self.permValue(request, c = 1) & 1):
			return super().list(request, args, kwargs)
		return Response({'status': 'Permission Denied'}, status=status.HTTP_403_FORBIDDEN)
	
	
	def retrieve(self, request, *args, **kwargs):
		if (self.permValue(request)):
			return super().retrieve(request, args, kwargs)
		return Response({'status': 'Permission Denied'}, status=status.HTTP_403_FORBIDDEN)
	
	
	def permValue(self, request, c = 3):
		perm = UserGroupPermission()
		retVal = 0
		if ( c & 1 and perm.has_permission(request, None)):
			retVal += 1
		
		if ( c & 2 and request.user == self.get_object()):
			retVal += 2
		return retVal

class GroupViewSet(viewsets.ModelViewSet, APIView):
	"""
	API endpoint that allows groups to be viewed or edited.
	"""
	queryset = Group.objects.all()
	serializer_class = GroupSerializer
	permission_classes = (IsAuthenticated, GroupPermission, )
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

	def delete_by_name(self, request, *args, **kwargs):
		group_name = kwargs.pop('group')
		content = {
			'status': 'Invalid Group name'
		}

		group = Group.objects.filter(name=group_name).first()
		if group:
			group.delete()
			content['status'] = 'Group is deleted'
		
		return Response(content, status=status.HTTP_200_OK)
