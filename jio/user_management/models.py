from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    GENDER = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other'),
        ('N', 'Not Willing to mention'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name = 'profile')
    email = models.EmailField(max_length=100, unique=True)
    phone_no = models.TextField(max_length=20, blank=True)
    address = models.TextField(max_length=500, blank=True)
    gender = models.CharField(max_length=1, choices=GENDER, default='N')
    #email(unique), name, phoneno, address, gender

'''
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
'''