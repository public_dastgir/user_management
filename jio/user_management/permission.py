from rest_framework import permissions
from django.contrib.auth.models import Permission, User, Group
from django.urls import resolve

def parse_permission(perm_list, url, request):
    # Requires Logged in user
    print(url)
    if (not request.user or url not in perm_list):
        return False
    elif (type(perm_list[url]) == str and perm_list[url] == ''):
        return True
    elif (request.method not in perm_list[url]):
        return False
    elif (type(perm_list[url][request.method]) == str and perm_list[url][request.method] == ''):
        return True
    elif (type(perm_list[url][request.method]) == list):
        for perms in perm_list[url][request.method]:
            if (not request.user.has_perm(perms)):
                return False
        return True
    elif (request.user.has_perm(perm_list[url][request.method])):
        return True
    return False

class GroupPermission(permissions.BasePermission):
    """
    Permission for group
    """

    def has_permission(self, request, view):
        url = resolve(request.path_info).url_name
        permission = {
            'group-list': {
                'GET': 'auth.list_group',
                'POST': 'auth.add_group',
            },
            # Detail
            'group-detail': {
                'GET': 'auth.list_group',
                'PUT': 'auth.change_group',
                #'PATCH': ''
                'DELETE': 'auth.delete_group',
            },
            'group-delete-detail': {
                'DELETE': 'auth.delete_group',
            },
        }
        return parse_permission(permission, url, request)

class UserPermission(permissions.BasePermission):
    """
    User Permissions
    """

    def has_permission(self, request, view):
        url = resolve(request.path_info).url_name
        permission = {
            'user-list': {
                'GET': 'auth.list_user', # Checks on runtime in views for permission 'auth.list_user'
                'POST': 'auth.add_user',
            },
            # Detail
            'user-detail': {
                'GET': 'auth.list_user',
                'PUT': 'auth.change_user',
                #'PATCH': ''
                'DELETE': 'auth.delete_user',
            },
            'user-suspend-detail': {
                'POST': 'auth.suspend_user',
                'PUT': ['auth.suspend_user', 'auth.unsuspend_user'],
                'DELETE': 'auth.unsuspend_user'
            },
            'user-delete-detail': {
                'GET': 'auth.delete_user'
            },
            'user-status': {
                'POST': ''
            }
        }
        print(parse_permission(permission, url, request))
        return parse_permission(permission, url, request)

class UserGroupPermission(permissions.BasePermission):
    """
    User Group Permissions
    """

    def has_permission(self, request, view):
        url = resolve(request.path_info).url_name
        permission = {
            # Detail
            'user-group-detail': {
                'POST': ['auth.assign_group', 'auth.unassign_group'],
                'PUT': 'auth.assign_group',
                'DELETE': 'auth.unassign_group',
            },
        }
        return parse_permission(permission, url, request)

class IsOwnerPermission(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        print("Test Perm2")
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in ['GET', 'HEAD', 'OPTIONS']:
            return True
        #
        # Instance must have an attribute named `owner`.
        
        if (type(obj) == User):
            print(obj, request.user, obj == request.user)
            return obj == request.user
        return obj.owner == request.user