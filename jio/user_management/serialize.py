from django.contrib.auth.models import User, Group, Permission
from rest_framework import serializers
from user_management.models import Profile
from drf_writable_nested import WritableNestedModelSerializer
from django.core.validators import EmailValidator
from django.db import models

class ProfileSerializer(serializers.ModelSerializer):
    old_password = serializers.CharField(required=False, style={'input_type': 'password'})
    new_password = serializers.CharField(required=False, style={'input_type': 'password'})
    """
    Serializer for Profile.
    """
    class Meta:
        
        model = Profile
        fields = ('pk', 'email', 'address', 'phone_no', 'gender', 'old_password', 'new_password')
        
        extra_kwargs = {
            'email': {
                'validators': [EmailValidator()],
            }
        }
    
    def create(self, validated_data):
        #print(validated_data.pop('new_password'))
        if 'new_password' in validated_data:
            validated_data.pop('new_password')
        if 'old_password' in validated_data:
            validated_data.pop('old_password')
        profile = Profile.objects.create(**validated_data)
        return profile
        

class UserSerializer(WritableNestedModelSerializer):
    profile = ProfileSerializer(required=True, many=False)
    #change_password = ChangePasswordSerializer(required=False)

    class Meta:
        model = User
        fields = ('url', 'first_name', 'last_name', 'profile')
        read_only_fields = ('username', 'groups', 'is_active')

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name', 'permissions')


class UserGroupAssignmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('url', 'groups')